package andika.yana.rentcar.View.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.Random;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditItemActivity extends AppCompatActivity implements View.OnClickListener {

    public static String INTENT_LIST = "intent_list";
    private Mobil mobil;
    @BindView(R.id.img_edit_item) ImageView imgEdit;
    @BindView(R.id.et_nama_mobil_edit) EditText etNama;
    @BindView(R.id.et_type_mobil_edit) EditText etType;
    @BindView(R.id.et_warna_mobil_edit) EditText etWarna;
    @BindView(R.id.et_harga_sewa_mobil_edit) EditText etHarga;
    @BindView(R.id.btn_edit_item) Button btnEdit;
    @BindView(R.id.btn_delete_item) Button btnDelete;

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase = null;
    private StorageReference storageReference = null;
    private DatabaseReference mDatabaseUser = null;
    private Uri uriImage = null;
    private Uri downloadUri = null;
    private static final int MAX_LENGTH = 7;
    private int requestMode = 1;
    private boolean isImageChange = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        ButterKnife.bind(this);

        mobil = getIntent().getParcelableExtra(INTENT_LIST);
        etNama.setText(mobil.getNamaMobil());
        etType.setText(mobil.getJenisMobil());
        etWarna.setText(mobil.getWarnaMobil());
        etHarga.setText(mobil.getHargaMobil());
        imgEdit.setPadding(0, 0, 0, 0);
        Glide.with(EditItemActivity.this)
                .load(mobil.getGambaMobil())
                .into(imgEdit);

        imgEdit.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_delete_item :
                deleteItem();
                break;
            case R.id.btn_edit_item :
                updateData();
                break;
            case R.id.img_edit_item :
                pickFromGallery();
                break;
        }
    }

    private void deleteItem(){
        final ProgressDialog dialog = new ProgressDialog(EditItemActivity.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Rentcar");
        Log.d("tag", "onMenuItemClick: " + myRef.child(mobil.getIdIklan()));
        myRef.child(mobil.getIdIklan()).removeValue();

        finish();
        dialog.hide();
    }

    private void updateData(){
        if(isImageChange){
            uploadImage();
        } else if(!isImageChange){
            final ProgressDialog dialog = new ProgressDialog(EditItemActivity.this);
            dialog.setMessage("Please Wait...");
            dialog.show();

            FirebaseDatabase database2 = FirebaseDatabase.getInstance();
            DatabaseReference myRef2 = database2.getReference().child("Rentcar").child(mobil.getIdIklan());
            myRef2.child("namaMobil").setValue(etNama.getText().toString());
            myRef2.child("jenisMobil").setValue(etType.getText().toString());
            myRef2.child("warnaMobil").setValue(etWarna.getText().toString());
            myRef2.child("hargaMobil").setValue(etHarga.getText().toString());
            myRef2.child("idIklan").setValue(mobil.getIdIklan());

            dialog.dismiss();
            finish();
        }
        Log.d("TAG", "IS Image Change : ");
    }

    private void uploadImage(){
        final ProgressDialog dialog = new ProgressDialog(EditItemActivity.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        storageReference = FirebaseStorage.getInstance().getReference();
        final StorageReference path = storageReference.child("RentCarImage");
        UploadTask uploadTask = path.putFile(uriImage);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return path.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    downloadUri = task.getResult();
                    Log.d("TAG", "cek link gambar : " + downloadUri);

                    FirebaseDatabase database2 = FirebaseDatabase.getInstance();
                    DatabaseReference myRef2 = database2.getReference().child("Rentcar").child(mobil.getIdIklan());
                    myRef2.child("namaMobil").setValue(etNama.getText().toString());
                    myRef2.child("jenisMobil").setValue(etType.getText().toString());
                    myRef2.child("warnaMobil").setValue(etWarna.getText().toString());
                    myRef2.child("hargaMobil").setValue(etHarga.getText().toString());
                    myRef2.child("idIklan").setValue(mobil.getIdIklan());
                    myRef2.child("gambaMobil").setValue(downloadUri.toString());

                    finish();
                    dialog.dismiss();

                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == requestMode) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    Toast.makeText(EditItemActivity.this, "Cannot Retevie Selected Image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestMode);
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = random();
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = uCrop.withAspectRatio(1, 1);
        uCrop.start(EditItemActivity.this);
    }

    public static String random(){
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;

        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            uriImage = resultUri;
            Log.d("ProfilAdmin", "handleCropResult: " + resultUri);
            Glide.with(this)
                    .load(resultUri)
                    .into(imgEdit);
            imgEdit.setPadding(0,0 , 0, 0);
            isImageChange = true;
        } else {
            Toast.makeText(EditItemActivity.this, "Cannot retrieve selected image", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("ProfilActivity", "handleCropError: ", cropError);
            Toast.makeText(EditItemActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(EditItemActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
        }
    }

}
