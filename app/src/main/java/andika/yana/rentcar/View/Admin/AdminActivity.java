package andika.yana.rentcar.View.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import andika.yana.rentcar.LoginActivity;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.ll_profile) LinearLayout btnProfile;
    @BindView(R.id.ll_history) LinearLayout btnHistory;
    @BindView(R.id.ll_item) LinearLayout btnItem;

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);

        chekFirebaseUser();

        getSupportActionBar().setTitle("Admin");

        btnProfile.setOnClickListener(this);
        btnHistory.setOnClickListener(this);
        btnItem.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_item :
                startActivity(new Intent(AdminActivity.this, ItemsActivity.class));
                break;
            case R.id.ll_profile :
                startActivity(new Intent(AdminActivity.this, ProfilAdminActivity.class));
                break;
            case R.id.ll_history :
                break;
        }
    }

    private void chekFirebaseUser(){
        auth = FirebaseAuth.getInstance();

        //set current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    //jiak user kosong kembali ke login
                    startActivity(new Intent(AdminActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }
}
