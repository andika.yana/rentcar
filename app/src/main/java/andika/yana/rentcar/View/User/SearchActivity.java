package andika.yana.rentcar.View.User;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import andika.yana.rentcar.Adapter.MobilUserAdapter;
import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.rv_search)
    RecyclerView rvSearch;

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private List<Mobil> list = new ArrayList<>();
    private MobilUserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Search");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadFirebase();
    }
    private void loadFirebase() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Rentcar");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Log.d("TAG", "onDataChange: " + dataSnapshot);

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Mobil item = snapshot.getValue(Mobil.class);
                    list.add(item);
                    adapter = new MobilUserAdapter(list);
                    rvSearch.setHasFixedSize(true);
                    rvSearch.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                    rvSearch.setAdapter(adapter);
                    dialog.hide();
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
