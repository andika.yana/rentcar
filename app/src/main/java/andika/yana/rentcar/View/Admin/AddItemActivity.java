package andika.yana.rentcar.View.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddItemActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.img_add_item) ImageView imgAddFoto;
    @BindView(R.id.et_nama_mobil) EditText etNamaMobil;
    @BindView(R.id.et_type_mobil) EditText etTypeMobil;
    @BindView(R.id.et_warna_mobil) EditText etWarnaMobil;
    @BindView(R.id.et_harga_sewa_mobil) EditText etHargaSewa;
    @BindView(R.id.btn_add_item)
    Button btnAddItem;

    private DatabaseReference mDatabase = null;
    private FirebaseAuth firebaseAuth = null;
    private StorageReference storageReference = null;
    private DatabaseReference mDatabaseUser = null;
    private FirebaseUser firebaseUser = null;
    private Uri uriImage = null;
    private Uri downloadUri = null;
    private Uri resultUri = null;
    private static final int MAX_LENGTH = 7;
    private int requestMode = 1;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private static final String TAG = "AddItemActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        ButterKnife.bind(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnAddItem.setOnClickListener(this);
        imgAddFoto.setOnClickListener(this);

        Glide.with(this)
                .load(R.drawable.ic_add_a_photo)
                .into(imgAddFoto);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_add_item :
                pickFromGallery();
                break;
            case R.id.btn_add_item:
                uploadData();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == requestMode) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    Toast.makeText(AddItemActivity.this, "Cannot Retevie Selected Image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestMode);
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = random();
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = uCrop.withAspectRatio(1, 1);
        uCrop.start(AddItemActivity.this);
    }

    public static String random(){
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;

        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    private void handleCropResult(@NonNull Intent result) {
        resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            uriImage = resultUri;
            Log.d("ProfilAdmin", "handleCropResult: " + resultUri);
            Glide.with(this)
                    .load(resultUri)
                    .into(imgAddFoto);
            imgAddFoto.setPadding(0,0 , 0, 0);
        } else {
            Toast.makeText(AddItemActivity.this, "Cannot retrieve selected image", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("ProfilActivity", "handleCropError: ", cropError);
            Toast.makeText(AddItemActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(AddItemActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
        }
    }

    public static String dateTime(){
        Date curDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String date = dateFormat.format(curDate);
        Log.d(TAG, "dateTime: ");
        return date;
    }


    private void uploadData(){
        final ProgressDialog dialog = new ProgressDialog(AddItemActivity.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Admin").child(firebaseUser.getUid());
        final String Nama = etNamaMobil.getText().toString().trim();
        final String Type = etTypeMobil.getText().toString().trim();
        final String Warna = etWarnaMobil.getText().toString().trim();
        final String Harga = etHargaSewa.getText().toString().trim();

        Log.d(TAG, "cek Database User: " + mDatabaseUser);

        if(Nama.equals("") && Type.equals("") && Warna.equals("")  && Harga.equals("")  && uriImage == null) {
            Toast.makeText(this, "Data ada yang kosong", Toast.LENGTH_SHORT).show();
        } else {

            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final StorageReference path = storageReference.child("RentCarImage").child(random());

            UploadTask uploadTask = path.putFile(uriImage);

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return path.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        downloadUri = task.getResult();
                        Log.d(TAG, "onComplete: URL " + downloadUri);

                        String Url = String.valueOf(downloadUri);
                        mDatabase = FirebaseDatabase.getInstance().getReference().child("Rentcar");
                        Mobil mobil = new Mobil();
                        mobil.setNamaMobil(Nama);
                        mobil.setJenisMobil(Type);
                        mobil.setWarnaMobil(Warna);
                        mobil.setHargaMobil(Harga);
                        mobil.setGambaMobil(downloadUri.toString());
                        mobil.setIdAdmin(user.getUid());
                        mobil.setIdIklan("");
                        mDatabase.push().setValue(mobil);

                        dialog.hide();
                        AddItemActivity.this.finish();
                        Toast.makeText(AddItemActivity.this, "Iklan Added!!", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(AddItemActivity.this, "Failed Add Data", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }


}
