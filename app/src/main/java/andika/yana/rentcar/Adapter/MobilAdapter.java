package andika.yana.rentcar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import andika.yana.rentcar.Util.FormatingToRupiah;
import andika.yana.rentcar.View.Admin.AddItemActivity;
import andika.yana.rentcar.View.Admin.EditItemActivity;
import andika.yana.rentcar.View.Admin.ItemsActivity;
import andika.yana.rentcar.View.User.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MobilAdapter extends RecyclerView.Adapter<MobilAdapter.MobilViewHolder>  {

    private List<Mobil> list;
    private MobilAdapter adapter;
    public MobilAdapter(List<Mobil> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MobilAdapter.MobilViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MobilViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull MobilAdapter.MobilViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MobilViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_mobil_list) ImageView imgMobil;
        @BindView(R.id.tv_nama_mobil_list) TextView tvNamaMobil;
        @BindView(R.id.tv_harga_list) TextView tvHargaList;
        @BindView(R.id.img_more) ImageView imgMore;

        public MobilViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Mobil mobil) {
            FormatingToRupiah formating = new FormatingToRupiah();
            Glide.with(itemView.getContext())
                    .load(mobil.getGambaMobil())
                    .into(imgMobil);
            tvNamaMobil.setText(mobil.getNamaMobil());
            tvHargaList.setText(formating.toRupiah(mobil.getHargaMobil()));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(itemView.getContext(), EditItemActivity.class);
                    i.putExtra(EditItemActivity.INTENT_LIST, mobil);
                    itemView.getContext().startActivity(i);
                }
            });

            imgMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popupMenu = new PopupMenu(itemView.getContext(), imgMore);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_list_admin, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            if(menuItem.getItemId() == R.id.menu_item_edit){
                                Toast.makeText(itemView.getContext(), "Edited : " + mobil.getIdIklan(), Toast.LENGTH_SHORT).show();
                            } else if(menuItem.getItemId() == R.id.menu_item_delete){
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference myRef = database.getReference().child("Rentcar");
                                Log.d("tag", "onMenuItemClick: " + myRef.child(mobil.getIdIklan()));
                                myRef.child(mobil.getIdIklan()).removeValue();
                            }
                            return true;
                        }
                    });
                    popupMenu.show();
                }
            });
        }
    }
}
