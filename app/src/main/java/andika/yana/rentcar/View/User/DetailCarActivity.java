package andika.yana.rentcar.View.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import andika.yana.rentcar.Util.FormatingToRupiah;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailCarActivity extends AppCompatActivity implements View.OnClickListener {

    public static String DETAIL_MOBIL = "detail_mobil";
    private Mobil mobil;

    @BindView(R.id.expandedImage) ImageView imgExpand;
    @BindView(R.id.tv_harga_detail) TextView tvHarga;
    @BindView(R.id.btn_book_now) Button btnBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_car);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mobil = getIntent().getParcelableExtra(DETAIL_MOBIL);
        Glide.with(this)
                .load(mobil.getGambaMobil())
                .into(imgExpand);
        getSupportActionBar().setTitle(mobil.getNamaMobil());
        FormatingToRupiah formater = new FormatingToRupiah();
        tvHarga.setText(formater.toRupiah(mobil.getHargaMobil()));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnBook.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_book_now){
            Intent i = new Intent(DetailCarActivity.this, BookingActivity.class);
            i.putExtra(BookingActivity.INTENT_BOOKING, mobil);
            startActivity(i);
        }
    }
}
