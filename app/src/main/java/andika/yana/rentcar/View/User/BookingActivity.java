package andika.yana.rentcar.View.User;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_nama_mobil_booking) TextView tvNama;
    @BindView(R.id.tv_sub_total) TextView tvSubTotal;
    @BindView(R.id.tv_total) TextView tvTotal;
    @BindView(R.id.btn_proses) Button btnProsses;

    public static final String INTENT_BOOKING = "intent_booking";
    private Mobil mobil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobil = getIntent().getParcelableExtra(INTENT_BOOKING);

        tvNama.setText(mobil.getNamaMobil());
        tvSubTotal.setText("Subtotal : " + mobil.getHargaMobil());
        tvTotal.setText("Total : " + mobil.getHargaMobil());

        btnProsses.setOnClickListener(this);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_proses){
            Intent i = new Intent(BookingActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Toast.makeText(this, "Booking Berhasil", Toast.LENGTH_SHORT).show();
            startActivity(i);
        }
    }
}
