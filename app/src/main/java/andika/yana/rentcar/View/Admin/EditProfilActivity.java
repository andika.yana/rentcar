package andika.yana.rentcar.View.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.Random;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfilActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.img_edit_admin) ImageView imgAdmin;
    @BindView(R.id.et_nama_admin_edit) EditText etNama;
    @BindView(R.id.et_email_admin_edit) EditText etEmail;
    @BindView(R.id.et_telepon_admin_edit) EditText etTelepon;
    @BindView(R.id.et_alamat_admin_edit) EditText etAlamat;
    @BindView(R.id.et_lokasi_admin_edit) EditText etLokasi;
    @BindView(R.id.et_password_admin_edit) EditText etPassword;
    @BindView(R.id.et_password_baru_admin_edit) EditText etPasswordBaru;
    @BindView(R.id.et_password_baru_ulang_admin_edit) EditText etPasswordBaru2;
    @BindView(R.id.btn_edit_admin) Button btnEdit;
    @BindView(R.id.btn_delete_admin) Button btnDelete;

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase = null;
    private StorageReference storageReference = null;
    private DatabaseReference mDatabaseUser = null;
    private Uri uriImage = null;
    private Uri downloadUri = null;
    private static final int MAX_LENGTH = 7;
    private int requestMode = 1;
    private boolean isImageChange = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ProgressDialog dialog = new ProgressDialog(EditProfilActivity.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        setContentView(R.layout.activity_edit_profil);
        ButterKnife.bind(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        imgAdmin.setOnClickListener(this);
        etLokasi.setOnClickListener(this);

        getData();

        dialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_edit_admin :
                //updateData();
                break;
            case R.id.btn_delete_admin :
                break;
            case R.id.img_edit_admin :
                isImageChange = true;
                pickFromGallery();
                break;
            case R.id.et_lokasi_admin_edit:
                startActivity(new Intent(EditProfilActivity.this, MapsActivity.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == requestMode) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    Toast.makeText(EditProfilActivity.this, "Cannot Retevie Selected Image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestMode);
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = random();
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = uCrop.withAspectRatio(1, 1);
        uCrop.start(EditProfilActivity.this);
    }

    public static String random(){
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;

        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            uriImage = resultUri;
            Log.d("ProfilAdmin", "handleCropResult: " + resultUri);
            Glide.with(this)
                    .load(resultUri)
                    .into(imgAdmin);
            imgAdmin.setPadding(0,0 , 0, 0);
        } else {
            Toast.makeText(EditProfilActivity.this, "Cannot retrieve selected image", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("ProfilActivity", "handleCropError: ", cropError);
            Toast.makeText(EditProfilActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(EditProfilActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
        }
    }


    private void getData(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Admin").child(firebaseUser.getUid());
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nama = String.valueOf((dataSnapshot.child("name").getValue()));
                String email = String.valueOf((dataSnapshot.child("email").getValue()));
                String telepon = String.valueOf((dataSnapshot.child("phonenumber").getValue()));
                String alamat = String.valueOf((dataSnapshot.child("alamat").getValue()));
                String lokasi = String.valueOf((dataSnapshot.child("lokasi").getValue()));
                String image = String.valueOf((dataSnapshot.child("image").getValue()));

                etNama.setText(nama);
                etEmail.setText(email);
                etTelepon.setText(telepon);
                etAlamat.setText(alamat);
                if(lokasi.equals("null")){
                    etLokasi.setText("Lokasi Belum ditentukan");
                } else {
                    etLokasi.setText(lokasi);
                }
                if(image.equals("null")){
                    imgAdmin.setPadding(0, 0, 0, 0);
                    imgAdmin.setImageResource(R.drawable.img_toko);
                } else {
                    Glide.with(EditProfilActivity.this).load(image).into(imgAdmin);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void updateData(){
        final ProgressDialog dialog = new ProgressDialog(EditProfilActivity.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Admin").child(firebaseUser.getUid());
        myRef.child("name").setValue(etNama.getText().toString());
        myRef.child("email").setValue(etEmail.getText().toString());
        myRef.child("phonenumber").setValue(etTelepon.getText().toString());
        myRef.child("alamat").setValue(etAlamat.getText().toString());
        myRef.child("lokasi").setValue(etLokasi.getText().toString());

        dialog.hide();
        finish();
    }

    private void uploadImage(){

        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Admin").child(firebaseUser.getUid());

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final StorageReference path = storageReference.child("RentCarImage").child(random());

        UploadTask uploadTask = path.putFile(uriImage);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return path.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    downloadUri = task.getResult();
                    Log.d("TAG", "onComplete: URL " + downloadUri);

                    String Url = String.valueOf(downloadUri);
                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Rentcar");
                    Mobil mobil = new Mobil();

                    mobil.setGambaMobil(downloadUri.toString());
                    mobil.setIdAdmin(user.getUid());
                    mobil.setIdIklan("");
                    mDatabase.push().setValue(mobil);

            }
        }

        });
    }
}
