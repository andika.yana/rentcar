package andika.yana.rentcar.View.Admin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import andika.yana.rentcar.Adapter.MobilAdapter;
import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsActivity extends AppCompatActivity {

    @BindView(R.id.rv_items_admin)
    RecyclerView rvItems;

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private List<Mobil> list = new ArrayList<>();
    private Mobil mobil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        ButterKnife.bind(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ItemsActivity.this, AddItemActivity.class);
                startActivity(i);
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFirebase();

    }

    public void loadFirebase() {
        list = new ArrayList<>();
        rvItems.setAdapter(null);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Rentcar");
        Query query = myRef.orderByChild("idAdmin").equalTo(String.valueOf(user.getUid()));

        // Read from the database
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //list.removeAll(list);
                Log.d("TAG", "onDataChange: " + dataSnapshot);

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Mobil item = snapshot.getValue(Mobil.class);
                    item.setIdIklan(snapshot.getKey());
                    Log.d("TAG", "getKey: " + snapshot.getKey());
                    list.add(item);
                    Log.d("TAG", "onDataChange: i " + snapshot);
                    MobilAdapter adapter = new MobilAdapter(list);
                    adapter.notifyDataSetChanged();
                    rvItems.setHasFixedSize(true);
                    rvItems.setLayoutManager(new LinearLayoutManager(ItemsActivity.this));
                    rvItems.setAdapter(null);
                    rvItems.setAdapter(adapter);
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

    }
}
