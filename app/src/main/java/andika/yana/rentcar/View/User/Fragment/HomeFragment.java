package andika.yana.rentcar.View.User.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import andika.yana.rentcar.Adapter.MobilAdapter;
import andika.yana.rentcar.Adapter.MobilUserAdapter;
import andika.yana.rentcar.LoginActivity;
import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import andika.yana.rentcar.View.Admin.ItemsActivity;
import andika.yana.rentcar.View.User.DetailCarActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment {

    @BindView(R.id.rv_home)
    RecyclerView rvHome;

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private List<Mobil> list = new ArrayList<>();
    private MobilUserAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, v);
        loadFirebase();
        return v;
    }

    private void loadFirebase() {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("Rentcar");

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //list.removeAll(list);
                Log.d("TAG", "onDataChange: " + dataSnapshot);

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Mobil item = snapshot.getValue(Mobil.class);
                    list.add(item);
                    adapter = new MobilUserAdapter(list);
                    rvHome.setHasFixedSize(true);
                    rvHome.setLayoutManager(new LinearLayoutManager(getContext()));
                    rvHome.setAdapter(adapter);
                    dialog.hide();
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

    }

}
