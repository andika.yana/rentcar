package andika.yana.rentcar.View.User.Fragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("ValidFragment")
public class DateFragment3 extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    TextView tvTanggal;
    TextView tvHari;
    TextView tvBulan;

    public DateFragment3(View view, View view2, View view3){
        this.tvTanggal =(TextView) view;
        this.tvHari=(TextView) view2;
        this.tvBulan=(TextView) view3;
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }


    public void onDateSet(DatePicker view, int year, int month, int day) {
        String tanggal = String.valueOf(day);
        String formatTahun = String.valueOf(year + "/" + (month + 1) + "/" + day);
        Date date = new Date(formatTahun);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        tvTanggal.setText(tanggal);
        tvHari.setText(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()));
        tvBulan.setText(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
    }
}
