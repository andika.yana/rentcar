package andika.yana.rentcar.View.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.Random;

import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilAdminActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_change_foto_admin) TextView tvChangeFoto;
    @BindView(R.id.img_toko) CircleImageView imgToko;
    @BindView(R.id.tv_nama_toko) TextView tvNamaToko;
    @BindView(R.id.tv_email_toko) TextView tvEmail;
    @BindView(R.id.tv_no_telp_toko) TextView tvNoTelp;
    @BindView(R.id.tv_alamat_toko) TextView tvAlamatToko;
    @BindView(R.id.tv_lokasi_toko) TextView tvLokasiToko;
    @BindView(R.id.btn_edit_profile_toko) Button btnEdit;
    @BindView(R.id.btn_logout_toko) Button btnLogout;

    private int requestMode = 1;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private FirebaseDatabase database;
    private DatabaseReference myUser;
    private StorageReference storageReference;
    private DatabaseReference mDatabaseUser;
    private FirebaseUser firebaseUser;
    private Uri uriImage = null;
    private static final int MAX_LENGTH = 7;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_admin);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cekData();

        tvChangeFoto.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_change_foto_admin :
                pickFromGallery();
                break;
            case R.id.btn_edit_profile_toko :
                startActivity(new Intent(ProfilAdminActivity.this, EditProfilActivity.class));
                break;
            case R.id.btn_logout_toko :
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(ProfilAdminActivity.this, AdminActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == requestMode) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    Toast.makeText(ProfilAdminActivity.this, "Cannot Retevie Selected Image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Log.d("ProfilAdmin", "handleCropResult: " + resultUri);
            Glide.with(this)
                    .load(resultUri)
                    .into(imgToko);
            uriImage = resultUri;
            uploadImage();
            cekData();
        } else {
            Toast.makeText(ProfilAdminActivity.this, "Cannot retrieve selected image", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("ProfilActivity", "handleCropError: ", cropError);
            Toast.makeText(ProfilAdminActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(ProfilAdminActivity.this, "Unexpected error", Toast.LENGTH_SHORT).show();
        }
    }

    private void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestMode);
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = uCrop.withAspectRatio(1, 1);
        uCrop.start(ProfilAdminActivity.this);
    }

    void cekData(){

        database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myUser = database.getReference().child("Admin/" + user.getUid());

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();

        myUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nama = String.valueOf((dataSnapshot.child("name").getValue()));
                String email = String.valueOf((dataSnapshot.child("email").getValue()));
                String telepon = String.valueOf((dataSnapshot.child("phonenumber").getValue()));
                String alamat = String.valueOf((dataSnapshot.child("alamat").getValue()));
                String lokasi = String.valueOf((dataSnapshot.child("lokasi").getValue()));
                String image = String.valueOf((dataSnapshot.child("image").getValue()));

                tvNamaToko.setText(nama);
                tvEmail.setText(email);
                tvNoTelp.setText(telepon);
                tvAlamatToko.setText(alamat);

                if(lokasi.equals("null")){
                    tvLokasiToko.setText("Lokasi Belum ditentukan");
                } else {
                    tvLokasiToko.setText(lokasi);
                }

                if(image.equals("null")){
                    imgToko.setImageResource(R.drawable.img_toko);
                } else {
                    Glide.with(ProfilAdminActivity.this).load(image).into(imgToko);
                }

                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void uploadImage(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myUser = database.getReference().child("Admin/" + user.getUid());
        final StorageReference path = storageReference.child("image").child(random());

        UploadTask uploadTask = path.putFile(uriImage);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return path.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Log.d("TAG", "onComplete: " + downloadUri);
                    String Url = String.valueOf(downloadUri);
                    myUser.child("image").setValue(Url);
                } else {
                    // Handle failures
                    // ...
                }
            }
        });
    }

    public static String random(){
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;

        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();

    }




}
