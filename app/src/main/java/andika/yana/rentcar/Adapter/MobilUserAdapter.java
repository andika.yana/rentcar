package andika.yana.rentcar.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import andika.yana.rentcar.Model.Mobil;
import andika.yana.rentcar.R;
import andika.yana.rentcar.Util.FormatingToRupiah;
import andika.yana.rentcar.View.User.DetailCarActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MobilUserAdapter extends RecyclerView.Adapter<MobilUserAdapter.MobilUserViewHolder>  {

    private List<Mobil> list;

    public MobilUserAdapter(List<Mobil> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MobilUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_rentcar, parent, false);
        return new MobilUserViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull MobilUserViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MobilUserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_mobil_user) ImageView imgMobil;
        @BindView(R.id.tv_nama_mobil_user) TextView tvNamaMobil;
        @BindView(R.id.tv_harga_user) TextView tvHargaList;

        public MobilUserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Mobil mobil) {
            FormatingToRupiah formating = new FormatingToRupiah();
            Glide.with(itemView.getContext())
                    .load(mobil.getGambaMobil())
                    .into(imgMobil);
            tvNamaMobil.setText(mobil.getNamaMobil());
            tvHargaList.setText(formating.toRupiah(mobil.getHargaMobil()));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(itemView.getContext(), DetailCarActivity.class);
                    i.putExtra(DetailCarActivity.DETAIL_MOBIL, mobil);
                    itemView.getContext().startActivity(i);
                }
            });
        }
    }
}
