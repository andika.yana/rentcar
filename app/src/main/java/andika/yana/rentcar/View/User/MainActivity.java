package andika.yana.rentcar.View.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import andika.yana.rentcar.LoginActivity;
import andika.yana.rentcar.R;
import andika.yana.rentcar.View.User.Fragment.HistoryOrderFragment;
import andika.yana.rentcar.View.User.Fragment.HomeFragment;
import andika.yana.rentcar.View.User.Fragment.ProfilFragment;
import andika.yana.rentcar.View.User.Fragment.SearchFragment;
import andika.yana.rentcar.Util.BottomNavigationMenuHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

//import andika.yana.rentcar.BottomNavigationViewHelper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String EXTRA_LOGIN = "extra_login";
    public static boolean isLogin = false;
    private FragmentManager fragmentManager;
    private Fragment fragment;


    @BindView(R.id.btn_navbar) BottomNavigationView navbar;
    @BindView(R.id.toolbar) Toolbar toolbar;

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference myUser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        chekFirebaseUser();

        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        isLogin = getIntent().getBooleanExtra(EXTRA_LOGIN, false);
        Log.d("TAG", "onCreate: " + isLogin);

        setNavBar();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            navbar.setSelectedItemId(R.id.menu_home);
        } else if (id == R.id.nav_search) {
            navbar.setSelectedItemId(R.id.menu_search);
        } else if (id == R.id.nav_history) {
            navbar.setSelectedItemId(R.id.menu_history);
        } else if (id == R.id.nav_profile) {
            navbar.setSelectedItemId(R.id.menu_profile);
        } else if (id == R.id.nav_share) {
            Toast.makeText(this, "Share Clicked", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_about) {
            Toast.makeText(this, "About Clicked", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setNavBar(){
        fragmentManager = getSupportFragmentManager();
        navbar.inflateMenu(R.menu.menu_nav_bar);
        BottomNavigationMenuHelper.disableShiftMode(navbar);
        navbar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menu_home :
                        fragment = new HomeFragment();
                        toolbar.setTitle("Home");
                        break;
                    case R.id.menu_search :
                        fragment = new SearchFragment();
                        toolbar.setTitle("Search");
                        break;
                    case R.id.menu_history:
                        fragment = new HistoryOrderFragment();
                        toolbar.setTitle("History");
                        break;
                    case R.id.menu_profile :
                        fragment = new ProfilFragment();
                        toolbar.setTitle("Profil");
                        break;
                }
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layour, fragment).commit();
                return true;
            }
        });
        navbar.setSelectedItemId(R.id.menu_home);


    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    private void chekFirebaseUser(){
        auth = FirebaseAuth.getInstance();
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    //jiak user kosong kembali ke login
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };



    }


}
