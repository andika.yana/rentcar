package andika.yana.rentcar.View.User.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.Random;

import andika.yana.rentcar.R;
import andika.yana.rentcar.View.User.EditUserActivity;
import andika.yana.rentcar.View.User.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ProfilFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.tv_change_foto) TextView tvChangeFoto;
    @BindView(R.id.tv_name) TextView tvName;
    @BindView(R.id.tv_email) TextView tvEmail;
    @BindView(R.id.tv_no_telp) TextView tv_noTelp;
    @BindView(R.id.img_profile) CircleImageView imgProfile;
    @BindView(R.id.btn_logout) Button btnLogOut;
    @BindView(R.id.btn_edit_profil_user) Button btnEdit;

    private int requestMode = 2; //1 for activity, 2 for fragment
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private StorageReference storageReference;
    private FirebaseDatabase database;
    private DatabaseReference mDatabaseUser;
    private FirebaseUser firebaseUser;
    private Uri uriImage = null;
    private DatabaseReference myUser;
    private static final int MAX_LENGTH = 7;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profil, container, false);
        ButterKnife.bind(this, v);

        tvChangeFoto.setOnClickListener(this);
        btnLogOut.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        cekData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_change_foto :
                pickFromGallery();
                break;
            case R.id.btn_logout :
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(getActivity(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                break;
            case R.id.btn_edit_profil_user :
                Intent in = new Intent(getContext(), EditUserActivity.class);
                startActivity(in);
                break;
        }
    }

    private void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), requestMode);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == requestMode) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    Toast.makeText(getContext(), "Cannot Retevie Selected Image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
                //uriImage = data.getData();

            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Log.d("ProfilAdmin", "handleCropResult: " + resultUri);
            Glide.with(this)
                    .load(resultUri)
                    .into(imgProfile);
            uriImage = resultUri;
            uploadImage();
            cekData();
        } else {
            Toast.makeText(getContext(), "Cannot retrieve selected image", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("ProfilActivity", "handleCropError: ", cropError);
            Toast.makeText(getContext(), cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
        }
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getActivity().getCacheDir() ,destinationFileName)));
        uCrop = uCrop.withAspectRatio(1, 1);
        uCrop.start(getContext(), ProfilFragment.this);
    }

    void cekData(){

        database = FirebaseDatabase.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myUser = database.getReference().child("Users/" + user.getUid());

        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();

        myUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String nama = String.valueOf((dataSnapshot.child("name").getValue()));
                String email = String.valueOf((dataSnapshot.child("email").getValue()));
                String telepon = String.valueOf((dataSnapshot.child("phonenumber").getValue()));
                String image = String.valueOf((dataSnapshot.child("image").getValue()));



                tvName.setText(nama);
                tvEmail.setText(email);
                tv_noTelp.setText(telepon);
                Log.d("TAG", "cekImage: " + image);
                if(image.equals("null")){
                    imgProfile.setImageResource(R.drawable.img_people);
                } else {
                    Glide.with(getActivity()).load(image).into(imgProfile);
                }

                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void uploadImage(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myUser = database.getReference().child("Users/" + user.getUid());
        final StorageReference path = storageReference.child("image").child(random());

        UploadTask uploadTask = path.putFile(uriImage);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                 return path.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Log.d("TAG", "onComplete: " + downloadUri);
                    String Url = String.valueOf(downloadUri);
                    myUser.child("image").setValue(Url);
                } else {
                    // Handle failures
                    // ...
                }
            }
        });
    }

    public static String random(){
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;

        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();

    }

}
