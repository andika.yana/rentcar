package andika.yana.rentcar.View.Admin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import andika.yana.rentcar.LoginActivity;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupAdminActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btn_signup_signup_admin) Button btnSignup;
    @BindView(R.id.tv_signin_signup_admin) TextView tvSignin;
    @BindView(R.id.et_email_signup_admin) EditText etEmail;
    @BindView(R.id.et_name_signup_admin) EditText etName;
    @BindView(R.id.et_address_signup_admin) EditText etAddress;
    @BindView(R.id.et_password_signup_admin) EditText etPassword;
    @BindView(R.id.et_password_ulang_signup_admin) EditText etPassword2;
    @BindView(R.id.et_telepon_signup_admin) EditText etTelepon;
    @BindView(R.id.cb_signup_admin) CheckBox cbSignup;
    @BindView(R.id.tv_terms_of_service_admin) TextView tvTermOfService;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_admin);
        ButterKnife.bind(this);

        auth = FirebaseAuth.getInstance();

        btnSignup.setOnClickListener(this);
        tvSignin.setOnClickListener(this);
        tvTermOfService.setOnClickListener(this);

        getSupportActionBar().setTitle(R.string.signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_signup_signup_admin :
                validate();
                break;
            case R.id.tv_signin_signup_admin :
                finish();
                break;
            case R.id.tv_terms_of_service_admin :
                showTermOfService();
                break;
        }
    }

    private void validate() {
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();
        final String nama = etName.getText().toString().trim();
        String address = etAddress.getText().toString().trim();
        String password2 = etPassword2.getText().toString().trim();
        final String telepon = etTelepon.getText().toString().trim();

        if (email.equals("")) {
            Toast.makeText(this, R.string.error_email_empety, Toast.LENGTH_SHORT).show();
        } else if (nama.equals("")) {
            Toast.makeText(this, R.string.error_name_empety, Toast.LENGTH_SHORT).show();
        } else if (address.equals("")) {
            Toast.makeText(this, R.string.error_address_empety, Toast.LENGTH_SHORT).show();
        } else if (password.equals("")) {
            Toast.makeText(this, R.string.error_password_empety, Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(this, R.string.error_password_short, Toast.LENGTH_SHORT).show();
        } else if (password2.equals("")) {
            Toast.makeText(this, R.string.error_retype_password_empety, Toast.LENGTH_SHORT).show();
        } else if (!password.equals(password2)) {
            Toast.makeText(this, R.string.error_password_not_mach, Toast.LENGTH_SHORT).show();
        } else if (telepon.equals("")) {
            Toast.makeText(this, R.string.error_telepon_empety, Toast.LENGTH_SHORT).show();
        } else if (!cbSignup.isChecked()) {
            Toast.makeText(this, R.string.error_terms_of_service_checked, Toast.LENGTH_SHORT).show();
        } else {
            final ProgressDialog dialog = new ProgressDialog(SignupAdminActivity.this);
            dialog.setMessage("Please Wait...");
            dialog.show();
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(SignupAdminActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(SignupAdminActivity.this, "Pendaftaran Berhasil..", Toast.LENGTH_SHORT).show();

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Admin").child(user.getUid());
                        ref.child("email").setValue(email);
                        ref.child("password").setValue(password);
                        ref.child("name").setValue(nama);
                        ref.child("phonenumber").setValue(telepon);
                        ref.child("status").setValue("admin");
                        ref.child("image").setValue("null");
                        ref.child("lokasi").setValue("null");

                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(SignupAdminActivity.this, LoginActivity.class));
                        SignupAdminActivity.this.finish();
                    } else {
                        task.getException().printStackTrace();
                        dialog.dismiss();
                        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.error_signup, Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        });
                        snackbar.show();
                    }
                }
            });
        }
    }

    private void showTermOfService(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.terms_of_service);
        builder.setMessage(R.string.isi_terms_of_service);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.create().show();
    }

}
