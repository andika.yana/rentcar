package andika.yana.rentcar.View.User;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import andika.yana.rentcar.R;
import andika.yana.rentcar.View.Admin.EditProfilActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditUserActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.et_edit_user_nama) EditText etNama;
    @BindView(R.id.et_edit_user_email) EditText etEmail;
    @BindView(R.id.et_edit_user_telepon) EditText etTelepon;
    @BindView(R.id.et_edit_user_current_password) EditText etCurrentPassword;
    @BindView(R.id.et_edit_user_new_password) EditText etNewPassword;
    @BindView(R.id.et_edit_user_retype_password) EditText etRetypePassword;
    @BindView(R.id.btn_update_profile_user) Button btnUpdate;

    private FirebaseDatabase database;
    private DatabaseReference myUser;
    String nama = "", email = "", telepon = "", password = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        ButterKnife.bind(this);
        getData();
        btnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_update_profile_user) {
            updateData();
        }
    }

    private void getData(){
        database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myUser = database.getReference().child("Users/" + user.getUid());

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.please_wait));
        dialog.show();

        myUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nama = String.valueOf((dataSnapshot.child("name").getValue()));
                email = String.valueOf((dataSnapshot.child("email").getValue()));
                telepon = String.valueOf((dataSnapshot.child("phonenumber").getValue()));
                password = String.valueOf((dataSnapshot.child("password").getValue()));

                etNama.setText(nama);
                etEmail.setText(email);
                etTelepon.setText(telepon);
                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateData(){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        myUser = database.getReference().child("Users/" + user.getUid());

        myUser.child("name").setValue(etNama.getText().toString());
        myUser.child("email").setValue(etEmail.getText().toString());
        myUser.child("phonenumber").setValue(etTelepon.getText().toString());

        dialog.dismiss();
        //finish();

    }


}
