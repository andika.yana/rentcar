package andika.yana.rentcar.View.User.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Locale;

import andika.yana.rentcar.R;
import andika.yana.rentcar.View.User.SearchActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.spinner_search) Spinner spinner;
    @BindView(R.id.tv_tanggal) TextView tvTanggal;
    @BindView(R.id.tv_tanggal_3) TextView tvTanggal3;
    @BindView(R.id.tv_nama_hari) TextView tvNamaHari;
    @BindView(R.id.tv_nama_hari_3) TextView tvNamaHari3;
    @BindView(R.id.tv_bulan) TextView tvBulan;
    @BindView(R.id.tv_bulan_3) TextView tvBulan3;
    @BindView(R.id.ll_tanggal) LinearLayout llTanggal;
    @BindView(R.id.ll_tanggal_3) LinearLayout llTanggal3;
    @BindView(R.id.btn_search) Button btnSearch;

    private String TAG = "SearchFragment";

    DateFragment dateFragment;
    DateFragment3 dateFragment3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, v);

        setTanggal();
        llTanggal.setOnClickListener(this);
        llTanggal3.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        return v;
    }

    private void setTanggal(){
        Calendar cal = Calendar.getInstance();
        Calendar cal3 = Calendar.getInstance();
        cal3.add(Calendar.DATE, +3);

        Log.d(TAG, "setTanggal: " );
        Log.d(TAG, "setTanggal2: " );

        tvTanggal.setText(String.valueOf(cal.get(Calendar.DATE)));
        tvTanggal3.setText(String.valueOf(cal3.get(Calendar.DATE)));

        tvNamaHari.setText(cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()));
        tvNamaHari3.setText(cal3.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()));

        tvBulan.setText(cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        tvBulan3.setText(cal3.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_tanggal :
                dateFragment = new DateFragment(tvTanggal, tvNamaHari, tvBulan);
                dateFragment.show(getFragmentManager().beginTransaction(), "DatePicker");
                break;
            case R.id.ll_tanggal_3 :
                dateFragment3 = new DateFragment3(tvTanggal3, tvNamaHari3, tvBulan3);
                dateFragment3.show(getFragmentManager().beginTransaction(), "DatePicker");
                break;
            case R.id.btn_search :
                Intent i = new Intent(getContext(), SearchActivity.class);
                startActivity(i);
                break;
        }
    }

}
