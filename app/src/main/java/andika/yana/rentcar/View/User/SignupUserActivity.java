package andika.yana.rentcar.View.User;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import andika.yana.rentcar.LoginActivity;
import andika.yana.rentcar.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupUserActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btn_signup_signup) Button btnSignup;
    @BindView(R.id.tv_signin_signup) TextView tvSignin;
    @BindView(R.id.et_email_signup) EditText etEmail;
    @BindView(R.id.et_name_signup) EditText etName;
    @BindView(R.id.et_password_signup) EditText etPassword;
    @BindView(R.id.et_password_ulang_signup) EditText etPassword2;
    @BindView(R.id.et_telepon_signup) EditText etTelepon;
    @BindView(R.id.cb_signup) CheckBox cbSignup;
    @BindView(R.id.tv_terms_of_service) TextView tvTermOfService;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_user);
        ButterKnife.bind(this);

        auth = FirebaseAuth.getInstance();

        btnSignup.setOnClickListener(this);
        tvSignin.setOnClickListener(this);
        tvTermOfService.setOnClickListener(this);

        getSupportActionBar().setTitle(R.string.signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_signup_signup :
                validate();
                break;
            case R.id.tv_signin_signup :
                finish();
                break;
            case R.id.tv_terms_of_service :
                showTermOfService();
                break;
        }
    }

    private void validate() {
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();
        final String nama = etName.getText().toString().trim();
        String password2 = etPassword2.getText().toString().trim();
        final String telepon = etTelepon.getText().toString().trim();

        if (email.equals("")) {
            Toast.makeText(this, R.string.error_email_empety, Toast.LENGTH_SHORT).show();
        } else if (nama.equals("")) {
            Toast.makeText(this, R.string.error_name_empety, Toast.LENGTH_SHORT).show();
        } else if (password.equals("")) {
            Toast.makeText(this, R.string.error_password_empety, Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(this, R.string.error_password_short, Toast.LENGTH_SHORT).show();
        } else if (password2.equals("")) {
            Toast.makeText(this, R.string.error_retype_password_empety, Toast.LENGTH_SHORT).show();
        } else if (!password.equals(password2)) {
            Toast.makeText(this, R.string.error_password_not_mach, Toast.LENGTH_SHORT).show();
        } else if (telepon.equals("")) {
            Toast.makeText(this, R.string.error_telepon_empety, Toast.LENGTH_SHORT).show();
        } else if (!cbSignup.isChecked()) {
            Toast.makeText(this, R.string.error_terms_of_service_checked, Toast.LENGTH_SHORT).show();
        } else {
            final ProgressDialog dialog = new ProgressDialog(SignupUserActivity.this);
            dialog.setMessage("Please Wait...");
            dialog.show();
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(SignupUserActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(SignupUserActivity.this, "Pendaftaran Berhasil..", Toast.LENGTH_SHORT).show();

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users").child(user.getUid());
                        ref.child("email").setValue(email);
                        ref.child("password").setValue(password);
                        ref.child("name").setValue(nama);
                        ref.child("phonenumber").setValue(telepon);
                        ref.child("status").setValue("user");
                        ref.child("image").setValue("null");

                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(SignupUserActivity.this, LoginActivity.class));
                        SignupUserActivity.this.finish();
                    } else {
                        task.getException().printStackTrace();
                        dialog.dismiss();
                        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.error_signup, Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        });
                        snackbar.show();
                    }
                }
            });
        }

    }


    private void showTermOfService(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle(R.string.terms_of_service);
        builder.setMessage(R.string.isi_terms_of_service);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.create().show();
    }
}
