package andika.yana.rentcar.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Mobil implements Parcelable {

    private String namaMobil ;
    private String jenisMobil ;
    private String warnaMobil ;
    private String hargaMobil ;
    private String gambaMobil ;
    private String idAdmin ;
    private String idIklan ;


    public Mobil() {
    }

    public Mobil(String namaMobil, String jenisMobil, String warnaMobil, String hargaMobil, String gambaMobil, String idAdmin, String idIklan) {
        this.namaMobil = namaMobil;
        this.jenisMobil = jenisMobil;
        this.warnaMobil = warnaMobil;
        this.hargaMobil = hargaMobil;
        this.gambaMobil = gambaMobil;
        this.idAdmin = idAdmin;
        this.idIklan = idIklan;
    }

    public String getNamaMobil() {
        return namaMobil;
    }

    public void setNamaMobil(String namaMobil) {
        this.namaMobil = namaMobil;
    }

    public String getJenisMobil() {
        return jenisMobil;
    }

    public void setJenisMobil(String jenisMobil) {
        this.jenisMobil = jenisMobil;
    }

    public String getWarnaMobil() {
        return warnaMobil;
    }

    public void setWarnaMobil(String warnaMobil) {
        this.warnaMobil = warnaMobil;
    }

    public String getHargaMobil() {
        return hargaMobil;
    }

    public void setHargaMobil(String hargaMobil) {
        this.hargaMobil = hargaMobil;
    }

    public String getGambaMobil() {
        return gambaMobil;
    }

    public void setGambaMobil(String gambaMobil) {
        this.gambaMobil = gambaMobil;
    }

    public String getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(String idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getIdIklan() {
        return idIklan;
    }

    public void setIdIklan(String idIklan) {
        this.idIklan = idIklan;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.namaMobil);
        dest.writeString(this.jenisMobil);
        dest.writeString(this.warnaMobil);
        dest.writeString(this.hargaMobil);
        dest.writeString(this.gambaMobil);
        dest.writeString(this.idAdmin);
        dest.writeString(this.idIklan);
    }

    protected Mobil(Parcel in) {
        this.namaMobil = in.readString();
        this.jenisMobil = in.readString();
        this.warnaMobil = in.readString();
        this.hargaMobil = in.readString();
        this.gambaMobil = in.readString();
        this.idAdmin = in.readString();
        this.idIklan = in.readString();
    }

    public static final Creator<Mobil> CREATOR = new Creator<Mobil>() {
        @Override
        public Mobil createFromParcel(Parcel source) {
            return new Mobil(source);
        }

        @Override
        public Mobil[] newArray(int size) {
            return new Mobil[size];
        }
    };
}
