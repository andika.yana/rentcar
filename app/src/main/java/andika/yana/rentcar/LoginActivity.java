package andika.yana.rentcar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import andika.yana.rentcar.View.Admin.AdminActivity;
import andika.yana.rentcar.View.Admin.SignupAdminActivity;
import andika.yana.rentcar.View.User.MainActivity;
import andika.yana.rentcar.View.User.SignupUserActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.btn_login_login) Button btnLogin;
    @BindView(R.id.btn_signup_login) Button btnSignup;
    @BindView(R.id.et_email_login) AutoCompleteTextView etEmail;
    @BindView(R.id.et_password_login) EditText etPassword;
    @BindView(R.id.tv_lupa_password_login) TextView tvLupaPassword;
    @BindView(R.id.tv_signup_owner) TextView tvSignupOwner;

    String TAG = "LoginActivity";
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() != null){
            cekUser();
        }

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        btnLogin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);
        tvLupaPassword.setOnClickListener(this);
        tvSignupOwner.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login_login :
                validate();
                break;
            case R.id.btn_signup_login :
                startActivity(new Intent(LoginActivity.this, SignupUserActivity.class));
                break;
            case R.id.tv_lupa_password_login :
                Toast.makeText(this, "Lupa Password Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_signup_owner :
                startActivity(new Intent(LoginActivity.this, SignupAdminActivity.class));
                break;

        }
    }

    private void validate(){
        String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();

        if(email.equals("") && password.equals("")){
            Toast.makeText(this, R.string.error_email_password_empety, Toast.LENGTH_SHORT).show();
        } else if(email.equals("")){
            Toast.makeText(this, R.string.error_password_empety, Toast.LENGTH_SHORT).show();
        } else if(password.equals("")){
            Toast.makeText(this, R.string.error_password_empety, Toast.LENGTH_SHORT).show();
        } else if(password.length() < 6){
            Toast.makeText(this, R.string.error_password_short, Toast.LENGTH_SHORT).show();
        } else {
            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage(getString(R.string.please_wait));
            dialog.show();

            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                if (password.length() < 6) {
                                    Snackbar.make(findViewById(android.R.id.content), R.string.error_password_short, Snackbar.LENGTH_LONG).show();
                                } else {
                                    Snackbar.make(findViewById(android.R.id.content), R.string.error_cannot_login, Snackbar.LENGTH_LONG).show();
                                }
                            } else {
                                cekUser();
                            }
                            dialog.dismiss();
                        }
                    });
            }

    }

    private void cekUser(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final DatabaseReference myUser = database.getReference().child("Users/" + user.getUid());

        myUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    LoginActivity.this.finish();
                } else {
                    Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    LoginActivity.this.finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}

